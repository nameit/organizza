'use strict';

var app = app || {};

// equal width
app.directive('equalw', [
    function(){
        return {
            restrict: "AC",
            link: function(scope, element, attrs) {
                var w = ((window.innerWidth)/3.0);
                element.css("width",w +"px");
                if(attrs.style!=undefined)
                   element.css("border-right","1px solid #FFF");
            }
        }
    }
]);

app.directive('heightDashboard', [
    '$parse',
    function($parse){
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                var attr = $parse(attrs.heightDashboard);
                var rect = document.getElementById('top').getBoundingClientRect();
                var height = window.innerHeight - rect.height;
                var pubHeight = Math.ceil((100 / 640) * window.innerWidth);
                element.css("height", height +"px");
                scope.$watch(attr, function(newValue, oldValue) {
                    if(newValue)
                        element.css("height", (height-pubHeight) +"px");
                    else
                        element.css("height", height +"px");
                });
            }
        }
    }
]);

app.directive('heightGeral', [
    '$parse',
    function($parse){
        return {
            restrict: "A",
            link: function(scope, element, attrs) {

                var attr = $parse(attrs.heightGeral);
                var h = (window.device && window.device.platform === "iOS" && parseInt(window.device.version) >= 7)? 60:40;
                var height = _height - h;
                var pubHeight = Math.ceil((100 / 640) * window.innerWidth);
                element.css("height", height +"px");
                scope.$watch(attr, function(newValue, oldValue) {
                    
                    if(newValue)
                        element.css("height", (height-pubHeight) +"px");
                    else
                        element.css("height", height +"px");
                });
            }
        }
    }
]);

app.directive('heightMenu', [
    '$parse','$timeout',
    function($parse,$timeout){
        return {
            restrict: "A",
            link: function(scope, element, attrs) {

                var attr = $parse(attrs.heightMenu);
                var rect = document.getElementById('menu-top').getBoundingClientRect();
                //var height = window.innerHeight - rect.height;
                var height = window.innerHeight - 70;
                element.css("height", height +"px");
            }
        }
    }
]);

app.directive('heightSearch', [
    '$parse',
    function($parse){
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                var attr = $parse(attrs.heightSearch);
                var h = (window.device && window.device.platform === "iOS" && parseInt(window.device.version) >= 7)? 125:105;
                var height = window.innerHeight - h;

                var pb = Math.ceil((100 / 640) * window.innerWidth);
                /*element.css("height", height +"px");
                element.css("paddingBottom", pb +"px");*/

                scope.$watch(attr, function(newValue, oldValue) {
                    if(newValue){
                        var height = window.innerHeight - h;
                        element.css("height", height +"px");
                        element.css("paddingBottom", pb +"px");
                    }
                    else{
                        var height = window.innerHeight - h;
                        element.css("height", height +"px");
                        element.css("paddingBottom", "0px");
                    }
                });

            }
        }
    }
]);

// padding bot because pub
app.directive('paddingBottom', [
    '$parse',
    function($parse){
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                var parse = $parse(attrs.paddingBottom);
                var height = Math.ceil((100 / 640) * window.innerWidth);
                scope.$watch(parse, function(newValue) {
                    if(newValue!==null && newValue!==undefined && newValue!==""){
                        element.css("paddingBottom", height +"px");
                    }
                });
            }
        }
    }
]);

app.directive('pub', [
    '$rootScope', 'PubService', '$filter',
    function($rootScope, PubService, $filter){

        function link($scope, element, attrs ) {

            var height = Math.ceil((100 / 640) * window.innerWidth);

            $scope.pub = {
                img:null,
                url:null
            }

            PubService.getAll().then(function(data){

                if(data){
                    var pubs = data.data;
                    var pub = pubs[Math.floor(Math.random()*pubs.length)];

                    if (pub) {

                        $scope.pub.img = {
                            dir: 'pub-' + pub.id,
                            uri: pub.image,
                            filename: $filter('filename')(pub.image)
                        }

                        $scope.pub.url = pub.url;
                        element.css("height", height +"px");
                        $rootScope.hasPub = true;
                    }
                }
            });
        }

        return {
            restrict: "E",
            replace: true,
            template: '<div class="pub"><a ng-click="pubOpenUrl(pub.url)"><img src="" mm-load-img="pub.img" alt="" /></a></div>',
            link: link
        }
    }
]);

// menu
app.directive('menu', [
    '$rootScope', 'CategorysService',
    function($rootScope, CategorysService){

        function link($scope, element, attrs ) {

            setTimeout(function(){
                element.addClass('animate-menu');
            });

            var loadItems = function(){
                CategorysService.getMenu().then(function(items){
                    $rootScope.$safeApply($scope, function(){
                        $scope.items = items;
                    });
                });
            };

            loadItems();
        }

        return {
            restrict: "E",
            replace: true,
            templateUrl: 'partials/menu.html',
            link: link
        }
    }
]);

// overlaymenu
app.directive('overlaymenu', [
    function(){

        function link( $scope, element, attrs ) {
            setTimeout(function(){
                element.addClass('animate-overlaymenu');
            });
        }

        return {
            restrict: "E",
            replace: true,
            template: '<div class="overlaymenu ng-cloak" ng-show="main.menuVisible" ng-click="main.menuVisible = ! main.menuVisible"></div>',
            link: link
        }
    }
]);

app.directive('scrollerMenuHeight', [
    function(){

        var winHeight = window.innerHeight;
        var height = (winHeight - 65);

        function link( $scope, element, attrs ) {
            element.css("height", height +"px");
        }

        return {
            restrict: "A",
            link: link
        }
    }
]);

// debug
app.directive('debug', [
    function(){
        return {
            restrict: "E",
            templateUrl: 'partials/debug.html',
            controller: function($scope){

                window.device = window.device || {};

                $scope.devicePixelRatio = window.devicePixelRatio || '';
                $scope.deviceModel = window.device.model || '';
                $scope.devicePlatform = window.device.platform || '';
                $scope.deviceUuid = window.device.uuid || '';
                $scope.deviceVersion = window.device.version || '';
                $scope.deviceHeight = window.innerHeight;
                $scope.deviceWidth = window.innerWidth;
            }
        }
    }
]);

// imagefeatured
/*app.directive('imagefeatured', [
    '$rootScope',
    function($rootScope){

        function link($scope, element, attrs ) {


        }

        return {
            restrict: "E",
            replace: true,
            template: '<img src="img/slideshow_default.png" ng-src="img/slideshow_default.png" />',
            link: link
        }
    }
]);*/

// load img
var mmLoadImg = function($parse, $document, FS){

    function link($scope, element, attrs) {

        var record = $parse(attrs.mmLoadImg);
        var loadImg = function(uri){
            var img = $document[0].createElement('img');
            img.onerror = function (message) {
                /*console.log('Error Loaded');
                console.log(message);*/
            };
            img.onload = function () {
                attrs.$set('src', this.src);
            };
            img.src = uri;
        };

        $scope.$watch(record, function(newValue) {
            if(newValue!=undefined){
                FS().then(
                    function (fs) {
                        var uri = fs.toURL() + newValue.dir + '/' + newValue.filename;
                        
                        window.resolveLocalFileSystemURL(uri, function (fileEntry) {
                            fileEntry.file(function(f)
                            {
                                loadImg(uri+'?t='+f.lastModifiedDate);
                            });
                        }, function () 
                        {
                            console.log('Not Found: ' + uri);
                        });
                        
                    },
                    function () {
                        loadImg(newValue.uri);
                    }
                );
            }
        });
    }

    return {
        restrict: "A",
        link: link
    }
}

mmLoadImg.$inject = ['$parse','$document','FS'];

app.directive('mmLoadImg', mmLoadImg);

var gmap = function(GoogleMaps){

    function link($scope, element, attrs) {


        GoogleMaps.get().then(function(){

            var mapOptions,
                latitude = attrs.lat,
                longitude = attrs.lng,
                map, marker, latlng;

            /*latitude = latitude && parseFloat(latitude, 10) || 38.725299;
            longitude = longitude && parseFloat(longitude, 10) || -9.150036;*/
            latitude = latitude && parseFloat(latitude, 10) || 41.156689;
            longitude = longitude && parseFloat(longitude, 10) || -8.623925;

            latlng = new google.maps.LatLng(latitude, longitude);

            mapOptions = {
                zoom: 14,
                center: latlng,
                mapTypeControl: false,
                panControl: false,
                zoomControl: false,
                scaleControl: false,
                streetViewControl: false
            };

            map = new google.maps.Map(element[0], mapOptions);
            marker = new google.maps.Marker({ position: latlng, map: map});

            $scope.$watch('lat + lng', function(newValue, oldValue){
                latlng = new google.maps.LatLng($scope.lat, $scope.lng);
                marker.setPosition(latlng);
                map.setCenter(latlng);
            });

        });

    }

    return {
        restrict: "E",
        link: link,
        replace: true,
        scope: {
            lat: '@',
            lng: '@'
        },
        template: '<div class="gmap"></div>'

    }
};

gmap.$inject = ['GoogleMaps'];

app.directive('gmap', gmap);

app.directive('compile', function($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                return scope.$eval(attrs.compile);
            },
            function(value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        );
    };
});

app.directive('addEvent', [
    '$parse',
    function($parse){
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                var attr = $parse(attrs.heightSearch);
                var h = (window.device && window.device.platform === "iOS" && parseInt(window.device.version) >= 7)? 125:105;
                var height = window.innerHeight - h;
                var pb = Math.ceil((100 / 640) * window.innerWidth);
                /*element.css("height", height +"px");
                 element.css("paddingBottom", pb +"px");*/

                scope.$watch(attr, function(newValue, oldValue) {
                    if(newValue){
                        var height = window.innerHeight - h;
                        element.css("height", height +"px");
                        element.css("paddingBottom", pb +"px");
                    }
                    else{
                        var height = window.innerHeight - h;
                        element.css("height", height +"px");
                        element.css("paddingBottom", "0px");
                    }
                });

            }
        }
    }
]);