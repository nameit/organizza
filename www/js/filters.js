app.filters = angular.module('app.filters', []);

app.filters.filter('filename', function(){
    return function(filename) {
        return filename.replace(/^.*[\\\/]/, '');

    }
});

app.filters.filter('Date2Array', function(){
    return function(date) {
        var parts = date.split(' ');
        return parts.length===2?parts[0].split("-").concat(parts[1].split(":")):[];
    }
});

app.filters.filter('trunc', function(){
    return function(str, n, useWordBoundary) {
        var toLong = str.length>n,
            s_ = toLong ? str.substr(0,n-1) : str;
        s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
        return  toLong ? s_ + ' ...' : s_;
    }
});