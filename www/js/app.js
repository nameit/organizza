'use strict';

var app = app || {};

var _loading = false;
var _loadingNotifications = false;
var _width = window.innerWidth;
var _height = window.innerHeight;
var _lastSyn = new Date();
var _debug = true;
var _lastBadgeNumber = 0;
var log = function(args){ if(_debug) console.log(args);}

app = angular.module('app', [
    'ngTouch',
    'ngRoute',
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'app.controllers',
    'app.services',
    'app.filters'
]);

app.config(['$routeProvider', '$httpProvider', '$provide',
    function ($routeProvider, $httpProvider, $provide) {

        $routeProvider.when('/insertcode', {
            templateUrl: 'partials/insertcode.html'
        });

        $routeProvider.when('/init', {
            templateUrl: 'partials/init.html',
            controller: function ($timeout, $rootScope, CompanyService) {
                $timeout(function () {
                    CompanyService.get().then(function(company){
                        if(company && company.data.id>0){
                            if(navigator.network.connection.type === Connection.NONE) {
                                $rootScope.goPage('/dashboard');
                            }
                            else{
                                $rootScope.goPage('/loading');
                            }

                        }
                        else{
                            if(navigator.network.connection.type === Connection.NONE){
                                $rootScope.go('/noconnection');
                            }
                            else{
                                $rootScope.goPage('/insertcode');
                            }

                        }
                    });

                }, 3000);
            }
        });

        $routeProvider.when('/loading', {
            templateUrl: 'partials/loading.html',
            controller: 'LoadingCtrl'
        });

        $routeProvider.when('/dashboard', {
            templateUrl: 'partials/dashboard.html',
            controller: 'DashboardCtrl'
        });

        $routeProvider.when('/categorys', {
            templateUrl: 'partials/categorys.html',
            controller: 'CategorysCtrl'
        });

        $routeProvider.when('/categorys/:id', {
            templateUrl: 'partials/category.html',
            controller: 'CategoryCtrl'
        });

        $routeProvider.when('/search', {
            templateUrl: 'partials/search.html',
            controller: 'SearchCtrl'
        });

        $routeProvider.when('/featured', {
            templateUrl: 'partials/featured.html',
            controller: 'FeaturedCtrl'
        });

        $routeProvider.when('/month', {
            templateUrl: 'partials/month.html',
            controller: 'MonthCtrl'
        });
               
        $routeProvider.when('/map/:id', {
            templateUrl: 'partials/map.html',
            controller: 'mapCtrl'
        });

        $routeProvider.when('/event/:id', {
            templateUrl: 'partials/detail.html',
            controller: 'detailCtrl'
        });

        $routeProvider.when('/notifications', {
            templateUrl: 'partials/notifications.html',
            controller: 'notificationsCtrl'
        });

        $routeProvider.when('/noconnection', {
            templateUrl: 'partials/noconnection.html',
            controller: 'noconnectionCtrl'
        });

        $routeProvider.otherwise({
            redirectTo: '/init'
        });

        $httpProvider.defaults.useXDomain = true;

        $provide.decorator('$exceptionHandler', ['$delegate', '$window', '$log', 'stacktraceService', 'API_URL', function ($delegate, $window, $log, stacktraceService, API_URL) {
            return function (exception, cause) {

                $delegate(exception, cause);

                try {

                    window.device = window.device || {};

                    var errorMessage = exception.toString();
                    var stackTrace = stacktraceService.print({ e: exception });

                    $.ajax({
                        type: "POST",
                        url: API_URL + '/log',
                        data: {
                            errorUrl: $window.location.hash,
                            errorMessage: errorMessage,
                            stackTrace: stackTrace,
                            cause: ( cause || "" ),
                            deviceModel: ( window.device.model || "" ),
                            devicePlatform: ( window.device.platform || "" ),
                            deviceVersion: ( window.device.version || "" )
                        }
                    });

                } catch ( loggingError ) {
                    $log.warn( "Error logging failed" );
                    $log.log( loggingError );
                }
            };
        }]);

    }
]);

app.run(function($rootScope, $location, CompanyService, LastSynService, DownloadAssets, EventsService, CategorysService, PubService, RestData, $q, PushNotifications, DeviceEvents, RestNotifications, NotificationService) {

    var updateNotifications = function()
    {
        var excludePaths = ['/init', '/insertcode', '/loading', '/notifications'];
        if($.inArray($location.path(), excludePaths)===-1 && _loadingNotifications===false){
            _loadingNotifications = true;
            if(PushNotifications.getToken()!="")
            {
                CompanyService.get().then(function(company){
                    if(company){
                        var params = {id: company.data.id, token:PushNotifications.getToken()};
                        RestNotifications.get(params).$promise.then(function(data){
                            _loadingNotifications = false;
                            if(data.state){
                                NotificationService.saveFromResource(data.data).then(function(records){

                                    var  c = 0;
                                    if (records) {
                                        records = records.data;
                                        var i, n = records.length;
                                        for (i = 0; i < n; i++) {
                                            var record = records[i];
                                            if (record.read !== 1)
                                                c++;
                                        }
                                    }

                                    $rootScope.setBadges(c);
                                    PushNotifications.setIconBadgeNumber(c);
                                });
                            }
                        }, function(){
                            _loadingNotifications = false;
                        });
                    }
                });
            }
        }
    };

    $rootScope.$on('notification', function(){
        updateNotifications();
    });

    $rootScope.$on('resume', function(){
        updateNotifications();
    });

    DeviceEvents.bindAllEvents();

    PushNotifications.init();

    $rootScope.$on('online', function(){
        if(! PushNotifications.getToken()){
            PushNotifications.init();
        }
    });


    $rootScope.$on('$routeChangeSuccess', function () {

        document.activeElement.blur();

        $rootScope.hasPub = false;

        var excludePaths = ['/init', '/insertcode', '/loading'];

        updateNotifications();

        if($.inArray($location.path(), excludePaths)===-1 && navigator.network.connection.type === Connection.NONE) {
            return;
        }

        if($.inArray($location.path(), excludePaths)===-1 && _loading===false){
            var now = new Date();
            var diffMs = (now - _lastSyn);
            var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);

            if(diffMins>=2){
            //if(true){

                _loading = true;
                _lastSyn = new Date();

                var successRestData = function (resource) {
                    if(resource.state){

                        CompanyService.save(resource.data.company);
                        LastSynService.save(resource.data.lastSyn);

                        DownloadAssets(resource.data).then(function(data){

                            var deferAll = [];

                            deferAll.push(EventsService.save(data.eventos));
                            deferAll.push(CategorysService.save(data.categorias_evento));
                            deferAll.push(PubService.save(data.pub_geral));

                            $q.all(deferAll).then(function(){
                                $rootScope.$broadcast('loadData');
                                _loading = false;
                            });

                        });

                    }
                    else{
                        _loading = false;
                    }
                };

                var failRestData = function (e) {
                    _loading = false;
                };


                CompanyService.get().then(function(company){

                    if(company){

                        LastSynService.get().then(function(lastSyn){
                            lastSyn = lastSyn || 0;
                            var params = {id: company.data.id, lastSyn:lastSyn, token:device.uuid};
                            RestData.get(params).$promise.then(successRestData, failRestData);
                        });
                    }
                });

            }
            else{

                _loading = false;
            }

        }

    })
});

// prod
if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
    app.constant({
        API_URL: 'http://nameit.pt/memoupdate/api'
    });
}
// dev
else{
    app.constant({
        API_URL: 'http://nameit.pt/memoupdate/api'
        //API_URL: 'http://nameit.dev/api'
    });
}