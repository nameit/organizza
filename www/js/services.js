'use strict';

var app = app || {};

app.services = angular.module('app.services', ['ngResource']);

app.services.factory('CheckPin', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/company/pin/:pin', {});
    }
]);

app.services.factory('RestDevice', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/device', {});
    }
]);

app.services.factory('RestNotifications', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/notifications/:id/:token', {});
    }
]);

app.services.factory('RestData', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/company/:id/:lastSyn/:token', {});
    }
]);

app.services.factory('CastVote', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/event/castvote/:idcompany/:idevent/:deviceuuid/:vote', {});
    }
]);

app.services.factory('CastClassification', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/event/castclassification/:idcompany/:idevent/:deviceuuid/:classificacao', {});
    }
]);

/*app.services.factory('CastComment', [
    '$resource', 'API_URL',
    function ($resource, apiUrl) {
        return $resource(apiUrl+'/event/castcomment/:idcompany/:idevent/:deviceuuid/:comment', {});
    }
]);*/

app.services.factory('Store', [
    '$q',
    function ($q) {

        var Services = {};
        var lawnchair = new Lawnchair({table:'organizza', adaptor:'webkit'}, function(){
            // FIXME: remove on production
            //this.nuke();
        });

        Services.lawnchair = lawnchair;

        Services.save = function(key, data){
            var defer = $q.defer();
            var currentTime = new Date().getTime();

            lawnchair.save({key:key, lastSync:currentTime, dataList: data}, function(records){
                defer.resolve({ data: records.dataList});
            });

            return defer.promise;
        }

        Services.get = function (key) {
            var defer = $q.defer();

            lawnchair.get(key, function (records) {
                var data = null;
                if (records) {
                    data = { data: records.dataList || false };
                }

                defer.resolve(data);

            });

            return defer.promise;
        }

        return Services;
    }
]);

app.services.factory(
    "Message",
    function () {
        return({
            alert: function(msg, title, buttonName, callback){
                var alertCallback = function(){if(callback){callback.call()}}
                if(navigator.notification){
                    navigator.notification.alert(msg, alertCallback, title, buttonName);
                }
                else{
                    alert(msg);
                    alertCallback();
                }
            }
        });
    }
);

app.services.factory(
    "stacktraceService", [
    function () {
        return({
            print: printStackTrace
        });
    }
]);

app.services.factory(
    "Moment", [
    function () {
        var services = {};

        services._formatParse = "YYYY-MM-DD HH:mm:ss";
        services._format = "dddd, [dia] DD [de] MMMM, YYYY, [ás] HH[:]mm";
        services._lang = "pt";

        services.getParseFormat = function(){
            return services._formatParse;
        };

        services.getFormat = function(){
            return services._format;
        };

        services.getLang = function(){
            return services._lang;
        };

        services.now = function(){
            return moment();
        };

        services.parse = function(date, format){
            var format = format || services.getParseFormat();
            var parse = moment(date, format);
            return parse.isValid()? parse: false;
        };

        services.parseAndFormat = function(date, format, outFormat){
            var format = format || services.getParseFormat();
            var outFormat = outFormat || services.getFormat();
            var _format = "";
            var parse = moment(date, format, services.getLang());
            if(parse){
                _format = parse.format(outFormat);
            }
            return _format;
        };

        services.isFinish = function(date){
            var isAfter = true;
            var now = services.now();
            if(typeof date === "string"){
                var parse = services.parse(date);
                if(parse) isAfter = now.isAfter(parse);
            }
            else{
                isAfter = now.isAfter(date);
            }

            return isAfter;
        };

        return services;
    }
]);

app.services.factory('FS',[
    '$q',
    function($q)
    {
        var fileSystem = null;

        return function () 
        {
            var defer = $q.defer();
            if(fileSystem)
            {
                defer.resolve(fileSystem);
            }
            else if (window.requestFileSystem) 
            {
                window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (fs) 
                {
                    fileSystem = fs;
                    defer.resolve(fs);
                }, 
                function (e) 
                {
                    defer.reject(e);
                });
            }
            else
            {
                defer.reject({});
            }

            return defer.promise;
        };
    }
]);

app.services.factory('DownloadAssetsCompany', [
    '$q', 'FS', '$filter',
    function ($q, FS, $filter) {

        return function (data) {

            var defer = $q.defer();
            var files = [];

            FS().then(function(fs){

                var download = function(){
                    if(files.length===0){
                        defer.resolve(data);
                        return;
                    }

                    var file = files.pop();

                    fs.getDirectory(file.dir, {create: true}, 
                        function(entry)
                        {
                            entry.setMetadata(null, null, { "com.apple.MobileBackup": 1});
                            var ft = new FileTransfer();
                            var target = entry.toURL() + '/' + file.filename;

                            ft.download(file.uri, target, function(entryFile){
                                download();
                                entryFile.setMetadata(null, null, { "com.apple.MobileBackup": 1});
                            });

                        },
                        function(error)
                        {
                            console.log(error.code);
                        }
                    );
                    
                };

                if (data.logo !="" && data.logo_no_retina !="") {
                    var file = window.devicePixelRatio>1?data.logo:data.logo_no_retina;
                    var file_size = window.devicePixelRatio>1?data.logo_size:data.logo_no_retina_size;
                    var filename = $filter('filename')(file);

                    files.push({
                        dir: 'company',
                        size: file_size,
                        uri: encodeURI(file),
                        filename: filename
                    });
                }

                if (data.logo_small !="" && data.logo_small_no_retina !="") {

                    var file = window.devicePixelRatio>1?data.logo_small:data.logo_small_no_retina;
                    var file_size = window.devicePixelRatio>1?data.logo_small_size:data.logo_small_no_retina_size;
                    var filename = $filter('filename')(file);

                    files.push({
                        dir: 'company',
                        size: file_size,
                        uri: encodeURI(file),
                        filename: filename
                    });
                }

                download();

            },
            function(){
                defer.resolve(data);
                return;
            });



            return defer.promise;
        }
    }
]);

app.services.factory('DownloadAssets', [
    '$q', '$filter', 'FS',
    function ($q, $filter, FS) {

        return function (data) {

            var defer = $q.defer();
            var files = [];


            FS().then(function(fs){

                var download = function(){
                    if(files.length===0){
                        defer.resolve(data);
                        return;
                    }

                    var file = files.pop();
                    fs.getDirectory(file.dir, {create: true}, 
                        function(entry)
                        {

                        var ft = new FileTransfer();
                        var target = entry.toURL() + file.filename;
                        entry.setMetadata(null, null, { "com.apple.MobileBackup": 1});
                        entry.getFile(file.filename, {create: false, exclusive: false},
                            
                            function (fileEntry) {

                                fileEntry.file(function (f) {
                                    if (f.size != file.size) {
                                        ft.download(file.uri, target, function (entry) {
                                            //console.log("download complete: " + entry.toURL());
                                            download();
                                        });
                                    }
                                    else {
                                        download();
                                    }
                                });

                            },
                            // file not exists
                            function () {
                                ft.download(file.uri, target, function () {
                                    download();
                                });
                            }
                        );
                    });
                };

                // company
                if (data.company) {
                    var company = data.company;
                    if (company.logo != "" && company.logo_no_retina != "") {
                        var file = window.devicePixelRatio > 1 ? company.logo : company.logo_no_retina;
                        var file_size = window.devicePixelRatio > 1 ? company.logo_size : company.logo_no_retina_size;
                        var filename = $filter('filename')(file);

                        files.push({
                            dir: 'company',
                            size: file_size,
                            uri: encodeURI(file),
                            filename: filename
                        });
                    }

                    if (company.logo_small != "" && company.logo_small_no_retina != "") {

                        var file = window.devicePixelRatio > 1 ? company.logo_small : company.logo_small_no_retina;
                        var file_size = window.devicePixelRatio > 1 ? company.logo_small_size : company.logo_small_no_retina_size;
                        var filename = $filter('filename')(file);

                        files.push({
                            dir: 'company',
                            size: file_size,
                            uri: encodeURI(file),
                            filename: filename
                        });
                    }
                }

                // eventos
                if (data.eventos.length) {
                    angular.forEach(data.eventos, function (value, key) {
                        var dir = 'events-'+ value.id;

                        // slideshow
                        if (value.slideshow != "") {

                            var file = window.devicePixelRatio > 1 ? value.slideshow : value.slideshow_no_retina;
                            var file_size = window.devicePixelRatio > 1 ? value.slideshow_size : value.slideshow_no_retina_size;

                            if(file!=="" && file!==undefined && file!==undefined ){
                                var filename = $filter('filename')(file);

                                files.push({
                                    dir: dir,
                                    size: file_size,
                                    uri: encodeURI(file),
                                    filename: filename
                                });
                            }

                        }

                        // listagem
                        if (value.listagem != "") {

                            var file = window.devicePixelRatio > 1 ? value.listagem : value.listagem;
                            var file_size = window.devicePixelRatio > 1 ? value.listagem_size : value.listagem_size;

                            if(file!=="" && file!==undefined && file!==undefined ){
                                var filename = $filter('filename')(file);

                                files.push({
                                    dir: dir,
                                    size: file_size,
                                    uri: encodeURI(file),
                                    filename: filename
                                });
                            }

                        }

                        // pub events
                        if (value.pub.length) {
                            angular.forEach(value.pub, function (v, k) {
                                var _dir = 'events-pub-'+ v.id;

                                if(v.image){
                                    var filename = $filter('filename')(v.image);
                                    var file_size = v.image_size;
                                    files.push({
                                        dir: _dir,
                                        size: file_size,
                                        uri: encodeURI(v.image),
                                        filename: filename
                                    });
                                }
                            });
                        }
                    });
                }

                if (data.pub_geral.length) {
                    angular.forEach(data.pub_geral, function (value, key) {

                        var dir = 'pub-' + value.id;
                        var file = value.image;
                        var file_size = value.image_size;


                        if(file!=="" && file!==undefined && file!==undefined ){
                            var filename = $filter('filename')(file);

                            files.push({
                                dir: dir,
                                size: file_size,
                                uri: encodeURI(file),
                                filename: filename
                            });
                        }

                    });
                }

                download();

            },
            function(){
                defer.resolve(data);
                return;
            });



            return defer.promise;
        }
    }
]);

app.services.factory('DeleteFiles', [
    '$q', '$filter', 'FS',
    function ($q, $filter, FS) {

        return function (data) {

            var defer = $q.defer();
            var deleteFiles = function (entry) 
            {
                var dr = entry.createReader();
                dr.readEntries(function (entries) {
                    var i;
                    var dirs = [];

                    for (i = 0; i < entries.length; i++) {
                        if(entries[i].name.indexOf('-')!==-1){
                            if($.inArray(entries[i].name, dirIds)===-1){
                                entries[i].removeRecursively();
                            }
                        }
                    }

                    defer.resolve();

                });
            };

            var dirIds = [];
            // events dirs
            if(data.eventos.length){
                angular.forEach(data.eventos, function(value){
                    dirIds.push('events-'+ value.id);
                    // pubs events dirs
                    if (value.pub.length) {
                        angular.forEach(value.pub, function (v, k) {
                            dirIds.push('events-pub-'+ v.id);
                        });
                    }
                });
            }

            // pubs dirs
            if (data.pub_geral.length) {
                angular.forEach(data.pub_geral, function (value, key) {
                    dirIds.push('pub-' + value.id);
                });
            }

            FS().then(function (fs) {
                    deleteFiles(fs);
                    defer.resolve();
                },
                function () {
                    defer.resolve();
                    return;
                }
            );


            return defer.promise;
        }
    }
]);

/* Models */
app.services.factory('LastSynService', [
    '$q', 'Store',
    function($q, Store){
        var services = {};

        services.get = function(){
            var defer = $q.defer();

            Store.get('lastSyn').then(function(record){
                if(record){
                    defer.resolve(record.data);
                }
                else
                    defer.resolve(null);
            });

            return defer.promise;
        }

        services.save = function(data){
            var defer = $q.defer();

            Store.save('lastSyn', data).then(function(records){
                defer.resolve(records);
            });

            return defer.promise;
        }

        return services;
    }
]);

app.services.factory('CompanyService', [
    '$q', 'Store',
    function($q, Store){
        var services = {};

        services._record = null;

        services.get = function(){
            var defer = $q.defer();
            if(services._record===null){
                Store.get('company').then(function(record){
                    services._record = record;
                    defer.resolve(services._record);
                });
            }
            else{
                defer.resolve(services._record);
            }

            return defer.promise;
        }

        services.save = function(data){
            var defer = $q.defer();

            Store.save('company', data).then(function(records){
                services._record = records;
                defer.resolve(records);
            });

            return defer.promise;
        }

        return services;
    }
]);

app.services.factory('EventsService',  [
    '$q', 'Store', 'Moment', '$filter',
    function($q, Store, Moment, $filter){
        var services = {};

        services.nCharList = Math.floor((window.innerWidth*30)/320);
        services.nCharFeatured = Math.floor((window.innerWidth*35)/320);

        services.getAll = function(){
            var defer = $q.defer();
            Store.get('events').then(function(records){
                defer.resolve(records);
            });
            return defer.promise;
        }

        services.getFeatured = function(){
            var defer = $q.defer();
            var rows = [];

            services.getAll().then(function(records){

                angular.forEach(records.data, function(record){
                    if(!Moment.isFinish(record.data_fim) && record.destaque==="1"){

                        var file = window.devicePixelRatio > 1 ? record.slideshow : record.slideshow_no_retina;
                        var filename = $filter('filename')(file);

                        record._nome = $filter('trunc')(record.nome, services.nCharFeatured);
                        record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                        record.slideshow_img = {
                            dir: 'events-' + record.id,
                            uri: file,
                            filename: filename
                        };

                        rows.push(record);
                    }
                });

                var tmpArray = [];
                var recordsOrder = [];

                angular.forEach(rows, function(v){
                    tmpArray[v.ordering] = v;
                });

                angular.forEach(tmpArray, function(v){
                    if(v!==null)
                        recordsOrder.push(v);
                });

                defer.resolve(recordsOrder);
            });

            return defer.promise;
        }

        services.getFeaturedList = function(){
            var defer = $q.defer();
            var rows = [];

            services.getFeatured().then(function(records){

                angular.forEach(records, function(record){

                    var parse = Moment.parse(record.data_inicio);
                    var month = parse.month()+1;
                    var day = parse.date();
                    var year = parse.year();
                    var key = year+"-"+month+"-"+day;

                    if(angular.isArray(rows[key])){
                        rows[key].push(record);
                    }
                    else{
                        rows[key] = [];
                        rows[key].push(record);
                    }
                });

                var _rows = [];
                for(var key in rows){

                    var parse = Moment.parseAndFormat(key, 'YYYY-MM-DD', 'dddd, [dia] DD [de] MMMM, YYYY');
                    _rows.push({data:parse, items:rows[key]});
                }

                defer.resolve(_rows);


            });

            return defer.promise;
        }

        services.getByCategorys = function(){
            var defer = $q.defer();
            var rows = [];



            services.getAll().then(function(records){
                var recordsOrderByDate = $filter('orderBy')(records.data, 'data_inicio');

                angular.forEach(recordsOrderByDate, function(record){

                    if(!Moment.isFinish(record.data_fim)){

                        var key = record.categoria;

                        var file = window.devicePixelRatio > 1 ? record.listagem : record.listagem;
                        var filename = $filter('filename')(file);

                        record._nome = $filter('trunc')(record.nome, services.nCharList);
                        record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                        record.img = {
                            dir: 'events-' + record.id,
                            uri: file,
                            filename: filename
                        };

                        if(angular.isArray(rows[key])){
                            rows[key].push(record);
                        }
                        else{
                            rows[key] = [];
                            rows[key].push(record);
                        }
                    }
                });

                var _rows = [];
                for(var key in rows){
                    _rows.push({categoria:key, items:rows[key]});
                }

                defer.resolve(_rows);

            });


            return defer.promise;
        }

        services.getByCategoryId = function(id){
            var defer = $q.defer();
            var rows = [];

            services.getAll().then(function(records){
                var _reocords = $filter('filter')(records.data, {idcategoria:id}, true);
                var recordsOrderByDate = $filter('orderBy')(_reocords, 'data_inicio');

                angular.forEach(recordsOrderByDate, function(record){

                    var parse = Moment.parse(record.data_inicio);

                    if(!parse) return;

                    var month = parse.month()+1;
                    var day = parse.date();
                    var year = parse.year();
                    var key = year+"-"+month+"-"+day;

                    if(!Moment.isFinish(record.data_fim)){

                        var file = window.devicePixelRatio > 1 ? record.listagem : record.listagem;
                        var filename = $filter('filename')(file);

                        record._nome = $filter('trunc')(record.nome, services.nCharList);
                        record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                        record.img = {
                            dir: 'events-' + record.id,
                            uri: file,
                            filename: filename
                        };

                        if(angular.isArray(rows[key])){
                            rows[key].push(record);
                        }
                        else{
                            rows[key] = [];
                            rows[key].push(record);
                        }
                    }
                });

                var _rows = [];
                for(var key in rows){

                    var parse = Moment.parseAndFormat(key, 'YYYY-MM-DD', 'dddd, [dia] DD [de] MMMM, YYYY');
                    _rows.push({data:parse, items:rows[key]});
                }

                defer.resolve(_rows);

            });

            return defer.promise;
        }

        services.getSearch = function(){
            var defer = $q.defer();
            var rows = [];


            services.getAll().then(function(records){
                var recordsOrderByDate = $filter('orderBy')(records.data, 'data_inicio');

                angular.forEach(recordsOrderByDate, function(record){

                    var parse = Moment.parse(record.data_inicio);

                    if(!parse) return;

                    var month = parse.month()+1;
                    var day = parse.date();
                    var year = parse.year();
                    var key = year+"-"+month+"-"+day;

                    if(!Moment.isFinish(record.data_fim)){

                        var file = window.devicePixelRatio > 1 ? record.listagem : record.listagem;
                        var filename = $filter('filename')(file);

                        record._nome = $filter('trunc')(record.nome, services.nCharList);
                        record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                        record.img = {
                            dir: 'events-' + record.id,
                            uri: file,
                            filename: filename
                        };

                        if(angular.isArray(rows[key])){
                            rows[key].push(record);
                        }
                        else{
                            rows[key] = [];
                            rows[key].push(record);
                        }
                    }
                });

                var _rows = [];
                for(var key in rows){

                    var parse = Moment.parseAndFormat(key, 'YYYY-MM-DD', 'dddd, [dia] DD [de] MMMM, YYYY');
                    _rows.push({data:parse, items:rows[key]});
                }

                defer.resolve(_rows);

            });

            return defer.promise;
        }

        services.getCurrentMonth = function(){
            var defer = $q.defer();
            var rows = [];

            services.getAll().then(function(records){

                var now = Moment.now();
                var currentMonth = now.month();
                var currentYear = now.year();

                var recordsOrderByDate = $filter('orderBy')(records.data, 'data_inicio');

                angular.forEach(recordsOrderByDate, function(record){

                    var parse = Moment.parse(record.data_inicio);

                    if(!parse) return;

                    var month = parse.month();
                    var year = parse.year();
                    var day = parse.date();

                    if(!Moment.isFinish(record.data_fim) && currentMonth == month && currentYear == year){


                        var file = window.devicePixelRatio > 1 ? record.listagem : record.listagem;
                        var filename = $filter('filename')(file);

                        record._nome = $filter('trunc')(record.nome, services.nCharList);
                        record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                        record.hours = parse.format('HH');
                        record.img = {
                            dir: 'events-' + record.id,
                            uri: file,
                            filename: filename
                        };

                        if(angular.isArray(rows[day])){
                            rows[day].push(record);
                        }
                        else{
                            rows[day] = [];
                            rows[day].push(record);
                        }
                    }
                });

                var _rows = [];
                angular.forEach(rows, function(items, day){
                    _rows.push({day:day, items:items});
                });

                defer.resolve(_rows);

            });

            return defer.promise;
        }

        services.getDashboard = function(){
            var defer = $q.defer();
            var rows = [];

            services.getAll().then(function(records){

                var now = Moment.now();
                var tomorrow = now.clone();
                tomorrow.add(1, 'day');

                var nowDay = now.date();
                var nowMonth = now.month();
                var nowYear = now.year();

                var tomorrowDay = tomorrow.date();
                var tomorrowMonth = tomorrow.month();
                var tomorrowYear = tomorrow.year();

                var recordsOrderByDate = $filter('orderBy')(records.data, 'data_inicio');


                angular.forEach(recordsOrderByDate, function(record){

                    var parse = Moment.parse(record.data_inicio);

                    if(!parse) return;

                    var month = parse.month();
                    var year = parse.year();
                    var day = parse.date();

                    if(!Moment.isFinish(record.data_fim)){

                        var file = window.devicePixelRatio > 1 ? record.listagem : record.listagem;
                        var filename = $filter('filename')(file);

                        record._nome = $filter('trunc')(record.nome, services.nCharList);
                        record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                        record.img = {
                            dir: 'events-' + record.id,
                            uri: file,
                            filename: filename
                        };


                        if(nowDay == day && nowMonth == month && nowYear == year){
                            if(angular.isArray(rows['Hoje'])){
                                rows['Hoje'].push(record);
                            }
                            else{
                                rows['Hoje'] = [];
                                rows['Hoje'].push(record);
                            }
                        }
                        else if(tomorrowDay == day && tomorrowMonth == month && tomorrowYear == year){
                            if(angular.isArray(rows['Amanhã'])){
                                rows['Amanhã'].push(record);
                            }
                            else{
                                rows['Amanhã'] = [];
                                rows['Amanhã'].push(record);
                            }
                        }
                        else{
                            if(angular.isArray(rows['Próximos Eventos'])){
                                rows['Próximos Eventos'].push(record);
                            }
                            else{
                                rows['Próximos Eventos'] = [];
                                rows['Próximos Eventos'].push(record);
                            }
                        }
                    }
                });

                rows['Próximos Eventos'] = $filter('limitTo')(rows['Próximos Eventos'], 4);

                var _rows = [];
                //var hasTodayTomorrow = (angular.isArray(rows['Hoje']) && rows['Hoje'].length>0) || (angular.isArray(rows['Amanhã']) && rows['Amanhã'].length>0);
                /*for(var _row in rows){

                    if(hasTodayTomorrow){
                        if("Próximos Eventos"!==_row){
                            _rows.push({day:_row, items:rows[_row]});
                        }
                    }
                    else
                        _rows.push({day:_row, items:rows[_row]});
                }*/
                var getOrder = function(order){
                    var r = 4;
                    if(order=="Hoje")
                        r = 1;
                    if(order=="Amanhã")
                        r = 2;
                    if(order=="Próximos Eventos")
                        r = 3;

                    return r;
                }

                for(var _row in rows){
                    _rows.push({day:_row, order: getOrder(_row), items:rows[_row]});
                }

                _rows = $filter('orderBy')(_rows, '+order');

                defer.resolve(_rows);

            });

            return defer.promise;
        }

        services.getById = function(id){
            var defer = $q.defer();

            services.getAll().then(function(records){

                var record = $filter('filter')(records.data, {id:id}, true)[0] || false;

                if(record){

                    var parse = Moment.parse(record.data_inicio);

                    record.hours = parse.format('HH');

                    record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);

                    var file = window.devicePixelRatio > 1 ? record.slideshow : record.slideshow_no_retina;
                    var filename = $filter('filename')(file);

                    record.img = {
                        dir: 'events-' + record.id,
                        uri: file,
                        filename: filename
                    };

                    defer.resolve(record);
                }
                else{
                    defer.reject();
                }
            });

            return defer.promise;
        }

        services.saveVotoByEventId = function(id,voto)
        {
            var defer = $q.defer();

            services.getAll().then(function(records)
            {
                var arrayData = false;
                if(records)
                    arrayData = records.data;

                if(arrayData)
                {

                    var newArray = [], i;
                    for(i=0; i<arrayData.length;i++){
                        var row = arrayData[i];
                        if(typeof row === 'string'){
                            row = JSON.parse(row);
                        }
                        if(row.id == id)
                            row.voto = voto;
                        newArray.push(row);
                    }
                    
                    services.save(newArray).then(function(records)
                    {
                        defer.resolve(records);
                    });
                }
            });

            return defer.promise;
        }

        services.saveClassifcacaoByEventId = function(id,classificacao)
        {
            var defer = $q.defer();

            services.getAll().then(function(records)
            {
                var arrayData = false;
                if(records)
                    arrayData = records.data;

                if(arrayData)
                {

                    var newArray = [], i;
                    for(i=0; i<arrayData.length;i++){
                        var row = arrayData[i];
                        if(typeof row === 'string'){
                            row = JSON.parse(row);
                        }
                        if(row.id == id)
                            row.classificacao = classificacao;
                        newArray.push(row);
                    }
                    
                    services.save(newArray).then(function(records)
                    {
                        defer.resolve(records);
                    });
                }
            });

            return defer.promise;
        }
        
        services.save = function(data){
            var defer = $q.defer();

            Store.save('events', data).then(function(records){
                services._records = records;
                defer.resolve(records);
            });

            return defer.promise;
        }

        return services;
    }
]);

app.services.factory('CategorysService', [
    '$q', 'Store', '$filter',
    function($q, Store, $filter){
        var services = {};

        services.getAll = function(){
            var defer = $q.defer();
            Store.get('categorys').then(function(records){
                defer.resolve(records);
            });

            return defer.promise;
        }

        services.getMenu = function(){
            var defer = $q.defer();

            services.getAll().then(function(records){
                if(records){
                    var tmpArray = [];
                    var recordsOrder = [];

                    //var recordsOrder = $filter('orderBy')(records.data, '+ordering');

                    angular.forEach(records.data, function(v){
                        tmpArray[v.ordering] = v;
                    });

                    angular.forEach(tmpArray, function(v){
                        if(v!==null)
                            recordsOrder.push(v);
                    });

                    defer.resolve(recordsOrder);
                }
                else{
                    defer.reject();
                }

            });

            return defer.promise;
        }

        services.getById = function(id){

            var defer = $q.defer();

            services.getAll().then(function(records){

                if(records){
                    var record = $filter('filter')(records.data, {id:id}, true)[0] || false;
                    defer.resolve(record);
                }
                else{
                    defer.reject();
                }

            });

            return defer.promise;
        }

        services.save = function(data){
            var defer = $q.defer();

            Store.save('categorys', data).then(function(records){
                services._records = records;
                defer.resolve(records);
            });

            return defer.promise;
        }

        return services;
    }
]);

app.services.factory('PubService', [
    '$q', 'Store',
    function($q, Store){
        var services = {};

        services.getAll = function(){
            var defer = $q.defer();
            Store.get('pub').then(function(records){
                defer.resolve(records);
            });

            return defer.promise;
        }

        services.save = function(data){
            var defer = $q.defer();

            Store.save('pub', data).then(function(records){
                defer.resolve(records);
            });

            return defer.promise;
        }

        return services;
    }
]);

app.services.factory('GoogleMaps', [
    '$q',
    function($q){
        var services = {};

        services.map = null;

        services.get = function(){
            var defer = $q.defer();

            if( window.google && google.maps ) {
                defer.resolve();
            }
            else{

                window.initialize = function(){
                    defer.resolve();
                }

                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=pt&callback=initialize';
                document.body.appendChild(script);
            }

            return defer.promise;
        };

        return services;
    }
]);

app.services.factory('NotificationService', [
    '$q','$filter','Store', 'EventsService', 'Moment',
    function($q, $filter, Store, EventsService, Moment){
        var services = {};

        services.nCharList = Math.floor((window.innerWidth*30)/320);

        services.get = function(){
            var defer = $q.defer();

            Store.get('notifications').then(function(records){
                defer.resolve(records);
            });

            return defer.promise;
        }

        services.getAll = function(){
            var defer = $q.defer();
            var events = [];

            Store.get('notifications').then(function(records){

                /*log('get notifications');
                log(JSON.stringify(records.data));*/

                var arrayNotifications = false;
                if(records)
                    arrayNotifications = records.data;

                if(arrayNotifications && angular.isArray(arrayNotifications)){

                    EventsService.getAll().then(function(records){

                        var arrayEvents = false;
                        if(records)
                            arrayEvents = records.data;

                        if(arrayEvents && angular.isArray(arrayEvents)){
                            var c = 0;
                            var ii;
                            for(ii=0;ii<arrayNotifications.length;ii++){
                                var row = arrayNotifications[ii];

                                if(typeof row === 'string'){
                                    row = JSON.parse(row);
                                }

                                var i;
                                var record = false;
                                for(i=0;i<arrayEvents.length;i++){
                                    var _row = arrayEvents[i];

                                    if(_row.id === row.id){
                                        record = angular.copy(_row);
                                        break;
                                    }
                                    else continue;
                                }

                                if (record) {

                                    c++;

                                    var file = window.devicePixelRatio > 1 ? record.listagem : record.listagem;
                                    var filename = $filter('filename')(file);

                                    record.img = {
                                        dir: 'events-' + record.id,
                                        uri: file,
                                        filename: filename
                                    };

                                    record._nome = $filter('trunc')(record.nome, services.nCharList);
                                    record.data_inicio_formated = Moment.parseAndFormat(record.data_inicio);
                                    record.formnow = moment.unix(row.time).fromNow();
                                    record.c = c;
                                    record.read = row.read || 0;
                                    record.type = row.type;
                                    record.time = row.time;

                                    events.push(record);
                                }
                            }
                        }

                        defer.resolve(events);

                    });
                }
                else{
                    defer.resolve(false);
                }

            });

            return defer.promise;
        }

        services.save = function(records){
            var defer = $q.defer();

            Store.save('notifications', records).then(function(records){
                defer.resolve(records);
            });

            return defer.promise;
        }

        services.saveFromResource = function(records){
            var defer = $q.defer();
            var dataArray = [];

            services.get().then(function(data){

                if(data){
                    var notifications = data.data;
                    var i,
                        ii,
                        recordsLen = records.length,
                        notificationsLen = notifications.length;

                    for(i=0; i<recordsLen;i++){

                        var record = records[i], notification = null;

                        if(typeof record === 'string'){
                            record = JSON.parse(record);
                        }

                        for(ii=0; ii<notificationsLen;ii++){

                            var notification = notifications[ii];
                            if(typeof notification === 'string'){
                                notification = JSON.parse(record);
                            }

                            if(record._id === notification._id){
                                record.read = notification.read || 0;
                                break;
                            }
                        }

                        dataArray.push(record);
                    }

                }
                else{
                    dataArray = records;
                }

                Store.save('notifications', dataArray).then(function(records){
                    defer.resolve(records);
                });

            });


            return defer.promise;
        }

        services.getBadgesNumber = function(){
            var defer = $q.defer();

            services.get().then(function(records){
                var count = 0;
                var arrayData = false;
                if(records)
                    arrayData = records.data;

                if(arrayData){
                    angular.forEach(arrayData, function(row){
                       if(row.read===0)
                        count++;
                    });
                }

                defer.resolve(count);
            });

            return defer.promise;
        }

        services.markAllRead = function(){
            var defer = $q.defer();

            services.get().then(function(records){
                var arrayData = false;
                if(records)
                    arrayData = records.data;

                if(arrayData){

                    var newArray = [], i;
                    for(i=0; i<arrayData.length;i++){
                        var row = arrayData[i];
                        if(typeof row === 'string'){
                            row = JSON.parse(row);
                        }
                        row.read = 1;
                        newArray.push(row);
                    }

                    services.save(newArray).then(function(records){
                        defer.resolve(records);
                    });
                }
            });

            return defer.promise;
        }

        return services;
    }
]);

app.services.factory('PushNotifications', [
    'CompanyService','NotificationService','$rootScope','RestDevice',
    function(CompanyService, NotificationService,$rootScope, RestDevice){
        var services = {};

        services.register = false;
        services.tagsRegister = false;

        services.pushNotification = window.plugins.pushNotification;

        services.push_token = '';
        services.hwid = '';
        services.timezone = '';
        services.language = '';

        services.init = function(){

            if(services.register){
                return;
            }

            

            if(device.platform==='Android')
            {
                services.pushNotification.onDeviceReady({ projectid: "384429062627", appid: "2C5AC-C146F" });
                androidInit();

            }
            else if(device.platform == "iPhone" || device.platform == "iOS")
            {
                services.pushNotification.onDeviceReady({alert: true, badge: true, sound: true, pw_appid: "2C5AC-C146F", appname: "Organizza"});
                iosInit();
            }
            else
                console.warn('Device no support.');
        };

        services.setIconBadgeNumber = function(number){
            if(device.platform == "iPhone" || device.platform == "iOS"){
                services.pushNotification.setApplicationIconBadgeNumber(number);
            }
        }

        services.clear = function(){
            if(device.platform == "iPhone" || device.platform == "iOS"){

                services.pushNotification.setApplicationIconBadgeNumber(0, function(status){
                    //log(JSON.stringify(["setApplicationIconBadgeNumber ", status]));
                });

                services.pushNotification.cancelAllLocalNotifications(function(status){
                    //log(JSON.stringify(["cancelAllLocalNotifications ", status]));
                });
            }
        }

        services.setCompanyTag = function(){

            CompanyService.get().then(function(company){
                if(company){
                    if(!services.tagsRegister){

                        var idcompany = company.data.id;
                        services.pushNotification.setTags({Company: idcompany},
                            function (status) {
                                console.warn('setTags success');

                                services.tagsRegister = true;

                                if(services.getToken()){
                                    var _RestDevice = new RestDevice();

                                    _RestDevice.idcompany = idcompany;
                                    _RestDevice.push_token = services.getToken();
                                    _RestDevice.device_model = window.device.model;
                                    _RestDevice.device_platform = window.device.platform;
                                    _RestDevice.device_version = window.device.version;

                                    _RestDevice.$save();
                                }
                            },
                            function (status) {
                                console.warn('setTags failed');
                            });
                    }
                }
            });
        };

        services.setToken = function(token){
            services.push_token = token;
            window.localStorage.push_token = token;
        }

        services.getToken = function(){
            return services.push_token?services.push_token: window.localStorage.push_token;
        }

        var androidInit = function(){
            bindPushNotification();
            services.pushNotification.registerDevice(successAndroid, fail);
        };

        var iosInit = function () {
            bindPushNotification();
            services.pushNotification.registerDevice(successIos, fail);
        };

        var bindPushNotification = function (){
            document.addEventListener('push-notification', function (event) {

                $rootScope.$broadcast('notification');
                $rootScope.icrementBadges();
            });
        }

        var successIos = function (status){
            services.register = true;

            var pushToken = status['deviceToken'];
            var hwid = status['hwid'];
            var timezone = status['timezone'];
            var language = status['language'];

            //log('registerDevice: ' + pushToken);

            services.setToken(pushToken);

            services.setCompanyTag();
        }

        var successAndroid = function (pushToken){

            services.register = true;

            services.setToken(pushToken);

            services.setCompanyTag();
            services.pushNotification.setMultiNotificationMode(
                function(status){
                    //console.log(JSON.stringify(['success to setMultiNotificationMode ', status]));
                },
                function(status){
                    //console.log(JSON.stringify(['failed to setMultiNotificationMode ', status]));
                }
            );
            services.pushNotification.setSoundType(2);
            services.pushNotification.setVibrateType(2);
        }

        var fail = function (status){
            console.log(JSON.stringify(['failed to register ', status]));
        }

        return services;
    }
]);

app.services.factory('hexToRgb', [
    function(){
        return function(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        }
    }
]);

app.services.factory('titlewidth', [
    function(){
        var w = Math.floor((window.innerWidth*40)/320);
        return {
            width: w + 'px'
        }
    }
]);

app.services.factory('DeviceEvents', [
    '$rootScope',
    function($rootScope){

        var services = {};

        services.bindAllEvents = function(){
            services.bindResume();
            services.bindActive();
            services.bindPause();
            services.bindOffline();
            services.bindOnline();
        }

        services.bindPause = function(){
            var callback = function(){
                setTimeout(function() {
                    $rootScope.$broadcast('pause');
                }, 0);
            };

            document.addEventListener("pause", callback, false);
        }

        services.bindResume = function(){
            var callback = function(){
                setTimeout(function() {
                    $rootScope.$broadcast('resume');
                }, 0);
            };

            document.addEventListener("resume", callback, false);
        }

        services.bindActive = function(){
            var callback = function(){
                setTimeout(function() {
                    $rootScope.$broadcast('active');
                }, 0);
            };

            document.addEventListener("active", callback, false);
        }

        services.bindOnline = function(){
            var callback = function(){
                setTimeout(function() {
                    $rootScope.$broadcast('online');
                }, 0);
            };

            document.addEventListener("online", callback, false);
        }

        services.bindOffline = function(){
            var callback = function(){
                setTimeout(function() {
                    $rootScope.$broadcast('offline');
                }, 0);
            };

            document.addEventListener("offline", callback, false);
        }

        return services;
    }
]);

