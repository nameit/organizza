var bootstrap = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {

        if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
            document.addEventListener("deviceready", this.onDeviceReady, false);
        } else {
            angular.element(document).ready(this.onDeviceReady);
        }
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        bootstrap.receivedEvent('deviceready');
        document.addEventListener("backbutton", bootstrap.exitApp, false);
    },
    exitApp: function(evt){
        evt.preventDefault();
        navigator.app.exitApp();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

        if (window.device && window.device.platform === "iOS") {
            document.body.className += ' ios';
        }

        if (window.device && window.device.platform === "Android") {
            document.body.className += ' android';
        }

        if (window.device && window.device.platform === "iOS" && parseInt(window.device.version) >= 7) {
            //document.body.style.marginTop = "20px";
            document.body.className += ' ios7';
        }

        if (window.device && window.device.platform === "iOS" && (parseFloat(window.device.version) === 6.1 || parseFloat(window.device.version) === 6.0)) {
            document.body.className += ' ios6';
        }

        if (window.device && window.device.platform === "iOS" && window.StatusBar){
            StatusBar.overlaysWebView(true);
            StatusBar.styleDefault();
            //StatusBar.styleLightContent();

        }

        // FIXME: Remover esta linha
        window.localStorage.push_token = '';

        setTimeout(function() {
            angular.bootstrap(document, ['app']);
            if(navigator.splashscreen)
                navigator.splashscreen.hide();
        }, 2000);

        //console.log('Received Event: ' + id);
    }
};
