'use strict';

var app = app || {};

app.controllers = angular.module('app.controllers', []);

app.controllers.controller('MainCtrl', [
    '$scope', '$rootScope', '$window',  '$timeout', '$location', 'CompanyService', 'CategorysService', '$filter',
    function ($scope, $rootScope, $window, $timeout, $location, CompanyService, CategorysService, $filter) {

        var self = this;
        var color;
        var iScrollMenu;

        this.logo = null;
        this.slide = '';
        this.menuVisible = false;

        this.badges = 0;

        var scroller = function(e) {
            e.preventDefault();
        };

        $scope.$watch(
            function () {
                return self.menuVisible;
            },
            function (newVal) {
                if (newVal) {

                    document.body.addEventListener('touchmove', scroller, false);

                    setTimeout(function () {
                        iScrollMenu = new IScroll('#wrapper-menu');
                    }, 100);
                }
                else{
                    document.body.removeEventListener('touchmove', scroller);
                }
            }
        );

        $rootScope.icrementBadges = function(){
            self.badges = self.badges + 1;
        }

        $rootScope.setBadges = function(number){
            self.badges = number;
        }

        $rootScope.hasPub = false;

        $rootScope.pubOpenUrl = function(url){
            var ref = window.open(encodeURI(url), '_system');
        };

        $rootScope.back = function () {

            var backAllowPath = ['map','event'];
            var parts = $location.path().split('/');

            if(parts.length>1){
                var path = parts[1] || "";
                if($.inArray(path, backAllowPath)!==-1) {
                    self.slide = 'slide-right';
                    $window.history.back();
                }
            }
        }

        $rootScope.goBack = function (path) {
            self.slide = 'slide-right';
            self.menu = false;
            $location.url(path);
        }

        $rootScope.go = function (path) {

            self.menuVisible = false;

            if (path != $location.path()) {
                self.slide = 'slide-left';
                $location.url(path);
            }
        }

        $rootScope.goPage = function (path) {

            self.menuVisible = false;

            if (path != $location.path()) {
                self.slide = '';
                $location.url(path);
            }
        }

        $rootScope.setSlide = function (slide) {
            self.slide = slide;
        }

        $rootScope.loading = function () {
            self.slide = 'slide-top';
            $location.path('/loading');
        }

        $rootScope.openUrl = function(url){
            window.open(url, '_system');
        }

        $rootScope.setColor = function (colorName) {
            color = colorName;
            self.styleColor = {color: colorName};
            self.styleBgColor = {background: colorName};
            self.styleBorderColor = {'border-color': colorName};
        }

        $rootScope.getColor = function () {
            return color;
        }

        $rootScope.setMenu = function (bol) {
            self.menuVisible = bol;
        }

        $rootScope.$safeApply = function ($scope, fn) {
            fn = fn || function () {
            };
            if ($scope.$$phase) {
                fn();
            }
            else {
                $scope.$apply(fn);
            }
        };

        var applyCompanyOptions = function () {
            CompanyService.get().then(function (company) {
                if (company) {
                    $rootScope.setColor(company.data.color);
                    $rootScope.$safeApply($scope, function () {
                        var file = window.devicePixelRatio > 1 ? company.data.logo_small : company.data.logo_small_no_retina;
                        self.logo = {
                            dir: 'company',
                            uri: file,
                            filename: $filter('filename')(file)
                        };
                    });
                }
            });
        };

        $rootScope.$on('loadData', function(){
            $rootScope.$safeApply($scope, function(){
                applyCompanyOptions();
            });
        });

        applyCompanyOptions();
    }
]);

app.controllers.controller('InsertCodeCtrl', [
    '$scope', '$rootScope', 'CheckPin', 'CompanyService', 'Message', 'DownloadAssetsCompany', 'LastSynService','PushNotifications','RestDevice','$timeout',
    function ($scope, $rootScope, CheckPin, CompanyService, Message, DownloadAssetsCompany, LastSynService, PushNotifications, RestDevice, $timeout) {

        if(navigator.network.connection.type === Connection.NONE){
            $rootScope.go('/noconnection');
        }

        $scope.$on('offline', function(){
            $timeout(function(){
                $rootScope.go('/noconnection');
            },100);
        });

        var self = this;

        this.loader = false;
        this.disabled = false;
        this.numbers = [];
        this.shake = false;

        // clean lastsysn
        LastSynService.save(0);

        if (device.platform == "Android") {
            var snd = new Media("/android_asset/www/touchtone0.mp3");
        } else {
            var snd = new Media("touchtone0.au");
        }


        this.number = function (n) {

            this.shake = false;

            if (this.numbers.length < 4) {
                this.numbers.push(n);
                snd.play();
            }

            if (this.numbers.length === 4) {
                var pin = this.numbers.toString().replace(/,/g, '');
                this.disabled = true;
                this.loader = true;
                if(pin=="1111" || pin=="1223")
                {
                  self.shake = true;
                  navigator.notification.vibrate(500);
                  self.clean();
                }
                else
                {
                    CheckPin.get({pin: pin}).$promise.then(
                    // susccess
                    function (resource) {
                        if (resource.state) {

                            //self.loader = false;

                            $rootScope.setColor(resource.data.color);

                            DownloadAssetsCompany(resource.data).then(function(){
                                CompanyService.save(resource.data).then(function(){
                                    PushNotifications.setCompanyTag();
                                    $rootScope.goPage('/loading');
                                });
                            });
                        }
                        else {
                            self.shake = true;
                            navigator.notification.vibrate(500);
                            self.clean();
                        }
                    },
                    // error
                    function (resource) {
                        self.loader = false;

                        var alertCallback = function () {

                            if(!$scope.$$phase) {
                                $rootScope.$safeApply($scope, function () {
                                    self.numbers = [];
                                    self.disabled = false;
                                });
                            }
                            else{
                                self.numbers = [];
                                self.disabled = false;
                            }
                        }
                        if(navigator.network.connection.type === Connection.NONE){
                            Message.alert("De momento não está conectado a internet. Por favor, verifique se tem ligação de internet.", "Mensagem", "Ok", alertCallback);
                        }
                        else{
                            Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok", alertCallback);
                        }
                    }
                );
              }
            }
        }

        this.clean = function () {
            this.numbers = [];
            this.disabled = false;
            this.loader = false;
        }

    }
]);

app.controllers.controller('LoadingCtrl', [
    '$scope', '$rootScope', 'RestData', 'Message', 'DownloadAssets', '$q', 'CompanyService', 'EventsService', 'CategorysService', 'PubService', '$filter', 'DeleteFiles', 'LastSynService',
    function ($scope, $rootScope, RestData, Message, DownloadAssets, $q, CompanyService, EventsService, CategorysService, PubService, $filter, DeleteFiles, LastSynService) {

        $scope.loading = {
            img: null
        };

        var successRestData = function (resource) {
            if(resource.state){

                CompanyService.save(resource.data.company);
                LastSynService.save(resource.data.lastSyn);

                DownloadAssets(resource.data).then(function(data){

                    var deferAll = [];

                    deferAll.push(EventsService.save(data.eventos));
                    deferAll.push(CategorysService.save(data.categorias_evento));
                    deferAll.push(PubService.save(data.pub_geral));
                    deferAll.push(DeleteFiles(resource.data));

                    $q.all(deferAll).then(function(){
                        _lastSyn = new Date();
                        $rootScope.$emit('loadData');
                        $rootScope.goPage('/dashboard');
                    });

                });

            }
            else{
                $rootScope.goPage('/dashboard');
            }
        };

        var alertCallback = function () {
            $rootScope.goBack('/insertcode');
            $rootScope.$digest();
        }

        var failRestData = function () {

            if(navigator.network.connection.type === Connection.NONE) {
                CompanyService.get().then(function(company){
                    if(company && company.data.id>0){
                        $rootScope.goPage('/dashboard');
                    }
                    else{
                        Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok", alertCallback);
                    }
                });
            }
            else{

                Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok", alertCallback);
            }
        };


        CompanyService.get().then(function(company){

            if(company){

                $rootScope.$safeApply($scope, function(){
                    var file = window.devicePixelRatio>1?company.data.logo:company.data.logo_no_retina;
                    $scope.loading.img = {
                        dir: 'company',
                        uri: file,
                        filename: $filter('filename')(file)
                    };

                });

                LastSynService.get().then(function(lastSyn){
                    //log('lastSyn: '+lastSyn);
                    lastSyn = lastSyn || 0;
                    var params = {id: company.data.id, lastSyn:lastSyn, token: device.uuid};
                    RestData.get(params).$promise.then(successRestData, failRestData);
                });
            }
        });



    }
]);

app.controllers.controller('DashboardCtrl', [
    '$scope', '$rootScope', 'EventsService', 'CompanyService', '$filter',
    function ($scope, $rootScope, EventsService, CompanyService, $filter) {

        $scope.events = {
            logo: null,
            featured: [],
            dashboard: []
        };

        EventsService.getFeatured().then(function(events){
            $rootScope.$safeApply($scope, function(){

                $scope.events.featured = events;


                var w = Math.ceil(window.innerWidth/events.length);
                var $indicator = $('#slider-indicator');
                $indicator.css('width', w+'px');

                setTimeout(function(){
                    CompanyService.get().then(function(company)
                    {
                        var autoVar=0;
                        if(company && company.data.banner_rotativo==1)
                            autoVar=4000;
                        window.mySwipe = new Swipe(document.getElementById('slider'), {
                            startSlide: 0,
                            speed: 400,
                            auto: autoVar,
                            continuous: true,
                            disableScroll: false,
                            stopPropagation: false,
                            callback: function(index) {
                                if(events.length===2){
                                    index = index===2?0:index;
                                    index = index===3?1:index;
                                }
                                $indicator.css('-webkit-transform', 'translate3d('+index*100+'%,0,0)');
                                
                            }
                        }); 
                    });
                }, 100);

            });
        });

        EventsService.getDashboard().then(function(events){
            $rootScope.$safeApply($scope, function(){
                $scope.events.dashboard = events;
            });
        });

        CompanyService.get().then(function(company){
            if(company){
                $rootScope.$safeApply($scope, function(){
                    var file = window.devicePixelRatio>1?company.data.logo_small:company.data.logo_small_no_retina;
                    $scope.events.logo = {
                        dir: 'company',
                        uri: file,
                        filename: $filter('filename')(file)
                    };
                });
            }
        });

    }
]);

app.controllers.controller('MenuCtrl', [
    '$scope', '$rootScope', '$location', 'CategorysService', 'CompanyService', '$filter',
    function ($scope, $rootScope, $location, CategorysService, CompanyService, $filter) {

        var self = this;
        var color = $rootScope.getColor();

        this.styleBorderColor = {borderColor: color};

        this.isSelectedLi = function(path){
            var color = $rootScope.getColor();
            return $location.path() == path ?
            //{backgroundColor: color, color:'#ffffff', borderColor: '#ffffff'} :
            {background: color, color:'#ffffff'} :
            {borderColor: color};
        }

        this.isSelectedSpan = function(path){
            return $location.path() == path ?
            {color:'#ffffff'} :
            {color: $rootScope.getColor()};
        }

        this.doSelected = function($event){
            var $element = $event.target.parentNode;
            $element.style.backgroundColor = $rootScope.getColor();
        }

        this.click = function($event, mainMenu){
            var $element = $event.target;

            if($element.nodeName==="DIV"){
                $rootScope.setMenu(false);
            }
        }

    }
]);

app.controllers.controller('CategorysCtrl', [
    '$scope', 'EventsService',
    function ($scope, EventsService) {
        $scope.events = {
            categorys: []
        };

        EventsService.getByCategorys().then(function(events){
            $scope.$safeApply($scope, function(){
                $scope.events.categorys = events;
            });
        });
    }
]);

app.controllers.controller('CategoryCtrl', [
    '$scope', '$rootScope', '$routeParams', 'EventsService', 'CategorysService',
    function ($scope, $rootScope, $routeParams, EventsService, CategorysService) {

        $scope.events = {
            category: "",
            categorys: []
        };

        EventsService.getByCategoryId($routeParams.id).then(function(events){
            $scope.$safeApply($scope, function(){
                $scope.events.categorys = events;
            });
        });

        CategorysService.getById($routeParams.id).then(function(category){
            if(category){
                $scope.events.category = category.nome;
            }
        });
    }
]);

app.controllers.controller('SearchCtrl', [
    '$scope', '$filter', 'EventsService',
    function ($scope, $filter, EventsService) {
        var eventsSearch = [];

        $scope.events = {
            search: [],
            pubShow: true
        };

        var search = function(value){
            var searchVal = $scope.q;
            if(searchVal == undefined || searchVal=="")
                return true;
            searchVal = searchVal.toLowerCase();
            
            if(value.nome.toLowerCase().indexOf(searchVal)>=0 ||
               value.texto.toLowerCase().indexOf(searchVal)>=0 ||
               value.texto2.toLowerCase().indexOf(searchVal)>=0)
                return true;
            else
                return false;
                                          
            //var objSearch = {nome: $scope.q};
            //return $filter('filter')([value], objSearch).length?true:false;
        };

        $scope.search = function (items) {

            var found = false;

            if(angular.isArray(items.items)){
                for(var item in items.items){
                    found = search(items.items[item]);
                    if(found) break;
                }
            }
            else{
                found = search(items)
            }

            return found;
        };

        EventsService.getSearch().then(function(events){
            $scope.$safeApply($scope, function(){
                eventsSearch = events;
                $scope.events.search = events;
            });
        });

        /*$scope.$watch('events.pubShow', function(newValue){
            if(!newValue){
                document.body.scrollTop = 0;
            }
        });*/
    }
]);

app.controllers.controller('FeaturedCtrl', [
    '$scope', 'EventsService',
    function ($scope, EventsService) {
        $scope.events = {
            featured: []
        };

        EventsService.getFeaturedList().then(function(events){
            $scope.$safeApply($scope, function(){
                $scope.events.featured = events;
            });
        });
    }
]);

app.controllers.controller('MonthCtrl', [
    '$scope', 'EventsService',
    function ($scope, EventsService) {
        $scope.events = {
            month: []
        };

        EventsService.getCurrentMonth().then(function(events){
            $scope.$safeApply($scope, function(){
                $scope.events.month = events;
            });
        });
    }
]);

app.controllers.controller('detailCtrl', [
    '$scope', '$rootScope', '$routeParams', 'EventsService', 'CompanyService', 'CastVote', 'CastClassification', 'Message', '$window', 'API_URL', '$filter', '$timeout', '$sce',
    function ($scope, $rootScope, $routeParams, EventsService, CompanyService, CastVote, CastClassification, Message, $window, API_URL, $filter, $timeout, $sce) {

        var addEvent = false;

        $scope.event = {
            detail: {},
            pub: {
                img: null,
                url: null
            }
        };

        var $elLoaderAddevent = $('#loader-addevent');

        EventsService.getById($routeParams.id).then(function(event){

            $timeout(function(){

                var pubs = event.pub;
                var pub = pubs[Math.floor(Math.random()*pubs.length)];


                if (pub) {

                    $scope.event.pub.img = {
                        dir: 'events-pub-' + pub.id,
                        uri: pub.image,
                        filename: $filter('filename')(pub.image)
                    }

                    $scope.event.pub.url = pub.url;
                    $scope.hasPub = true;
                }

                $scope.event.detail = event;

            }, 300);

        });

        $scope.hasComments = function (e)
        {
            var result = false;
            if(angular.isArray(e.detail.comentarios) && e.detail.comentarios.length>0)
                  result=true;
            return result;
        }
                                          
        $scope.printComment = function (coment)
        {
           return $sce.trustAsHtml("<p class=\"user_nome\">Por " + coment.nome + " em " + coment.data + "</p><p class=\"user_comentario\">" + coment.comentario + "</p>");
        }

        $scope.vote=function(id)
        {
            var elVote = angular.element(document.getElementById('btn-detalhe-votar'));
            if(elVote.hasClass("unchecked_vote"))
            {
                elVote.addClass("checked_vote");
                elVote.removeClass("unchecked_vote");
                $scope.voto = 1;
            }
            else
            {
                elVote.addClass("unchecked_vote");
                elVote.removeClass("checked_vote");
                $scope.voto = 0;
            }
            
            var successCastVote = function (resource) 
            {
                if(resource.state)
                {
                    if($scope.voto == 0)    
                        Message.alert("Voto removido com sucesso.", "Mensagem", "Ok");
                    else
                        Message.alert("Voto efectuado com sucesso.", "Mensagem", "Ok");
                    EventsService.saveVotoByEventId($scope.event.detail.id,$scope.voto);
                    $scope.event.detail.voto = $scope.voto;
                }
                else
                {
                    Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                    if($scope.voto == 0)
                    {
                        angular.element(document.getElementById('btn-detalhe-votar')).addClass("checked_vote");
                        angular.element(document.getElementById('btn-detalhe-votar')).removeClass("unchecked_vote");    
                    }
                    else
                    {
                        angular.element(document.getElementById('btn-detalhe-votar')).addClass("unchecked_vote");
                        angular.element(document.getElementById('btn-detalhe-votar')).removeClass("checked_vote");   
                    }
                }
            };
            var failCastVote = function () {
                Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                if($scope.voto == 0)
                {
                    angular.element(document.getElementById('btn-detalhe-votar')).addClass("checked_vote");
                    angular.element(document.getElementById('btn-detalhe-votar')).removeClass("unchecked_vote");    
                }
                else
                {
                    angular.element(document.getElementById('btn-detalhe-votar')).addClass("unchecked_vote");
                    angular.element(document.getElementById('btn-detalhe-votar')).removeClass("checked_vote");   
                }
            };
            
            CompanyService.get().then(function(company)
            {
                if(company)
                {
                    var params = {
                            idcompany: company.data.id, 
                            idevent: $scope.event.detail.id, 
                            deviceuuid: device.uuid, 
                            vote: $scope.voto
                        };
                    CastVote.get(params).$promise.then(successCastVote, failCastVote);        
                }
            });

            
        }

        
        $scope.star1=function(id)
        {
            var starEl1 = angular.element(document.getElementById('star1'));
            var starEl2 = angular.element(document.getElementById('star2'));
            var starEl3 = angular.element(document.getElementById('star3'));
            if($scope.event.detail.classificacao==1)
            {
                starEl1.attr("src","img/star.png");
                starEl2.attr("src","img/star.png");
                starEl3.attr("src","img/star.png");
                $scope.classificacao = 0;
            }
            else
            {
                starEl1.attr("src","img/star_selected.png");
                starEl2.attr("src","img/star.png");
                starEl3.attr("src","img/star.png");
                $scope.classificacao = 1;
            }

            var successCastClassificacao = function (resource) 
            {
                if(resource.state)
                {
                    if($scope.classificacao==0)
                        Message.alert("Classificação removida com sucesso.", "Mensagem", "Ok");
                    else
                        Message.alert("Classificação efectuada com sucesso.", "Mensagem", "Ok");
                    EventsService.saveClassifcacaoByEventId($scope.event.detail.id,$scope.classificacao);
                    $scope.event.detail.classificacao = $scope.classificacao;
                    
                }
                else
                {
                    Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                    if($scope.event.detail.classificacao == 0)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 1)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 2)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 3)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star_selected.png");
                    }
                }
            };

            var failCastClassificacao = function () {
                Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                if($scope.event.detail.classificacao == 0)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 1)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 2)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 3)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star_selected.png");
                }
            };
            
            CompanyService.get().then(function(company)
            {
                if(company)
                {
                    var params = {
                            idcompany: company.data.id, 
                            idevent: $scope.event.detail.id, 
                            deviceuuid: device.uuid, 
                            classificacao: $scope.classificacao
                        };
                    CastClassification.get(params).$promise.then(successCastClassificacao, failCastClassificacao);        
                }
            });
        }

        $scope.star2=function(id)
        {
            var starEl1 = angular.element(document.getElementById('star1'));
            var starEl2 = angular.element(document.getElementById('star2'));
            var starEl3 = angular.element(document.getElementById('star3'));
            if($scope.event.detail.classificacao==2)
            {
                starEl1.attr("src","img/star.png");
                starEl2.attr("src","img/star.png");
                starEl3.attr("src","img/star.png");
                $scope.classificacao = 0;
            }
            else
            {
                starEl1.attr("src","img/star_selected.png");
                starEl2.attr("src","img/star_selected.png");
                starEl3.attr("src","img/star.png");
                $scope.classificacao = 2;
            }

            var successCastClassificacao = function (resource) 
            {
                if(resource.state)
                {
                    if($scope.classificacao==0)
                        Message.alert("Classificação removida com sucesso.", "Mensagem", "Ok");
                    else
                        Message.alert("Classificação efectuada com sucesso.", "Mensagem", "Ok");
                    EventsService.saveClassifcacaoByEventId($scope.event.detail.id,$scope.classificacao);
                    $scope.event.detail.classificacao = $scope.classificacao;
                }
                else
                {
                    Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                    if($scope.event.detail.classificacao == 0)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 1)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 2)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 3)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star_selected.png");
                    }
                }
            };

            var failCastClassificacao = function () {
                Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                if($scope.event.detail.classificacao == 0)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 1)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 2)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 3)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star_selected.png");
                }
            };
            
            CompanyService.get().then(function(company)
            {
                if(company)
                {
                    var params = {
                            idcompany: company.data.id, 
                            idevent: $scope.event.detail.id, 
                            deviceuuid: device.uuid, 
                            classificacao: $scope.classificacao
                        };
                    CastClassification.get(params).$promise.then(successCastClassificacao, failCastClassificacao);        
                }
            });            
        }
        $scope.star3=function(id)
        {
            var starEl1 = angular.element(document.getElementById('star1'));
            var starEl2 = angular.element(document.getElementById('star2'));
            var starEl3 = angular.element(document.getElementById('star3'));
            if($scope.event.detail.classificacao==3)
            {
                starEl1.attr("src","img/star.png");
                starEl2.attr("src","img/star.png");
                starEl3.attr("src","img/star.png");
                $scope.classificacao = 0;
            }
            else
            {
                starEl1.attr("src","img/star_selected.png");
                starEl2.attr("src","img/star_selected.png");
                starEl3.attr("src","img/star_selected.png");
                $scope.classificacao = 3;
            } 

            var successCastClassificacao = function (resource) 
            {
                if(resource.state)
                {
                    if($scope.classificacao==0)
                        Message.alert("Classificação removida com sucesso.", "Mensagem", "Ok");
                    else
                        Message.alert("Classificação efectuada com sucesso.", "Mensagem", "Ok");
                    EventsService.saveClassifcacaoByEventId($scope.event.detail.id,$scope.classificacao);
                    $scope.event.detail.classificacao = $scope.classificacao;
                }
                else
                {
                    Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                    if($scope.event.detail.classificacao == 0)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 1)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 2)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star.png");
                    }
                    else if($scope.event.detail.classificacao == 3)
                    {
                        angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                        angular.element(document.getElementById('star3')).attr("src","img/star_selected.png");
                    }
                }
            };

            var failCastClassificacao = function () {
                Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                if($scope.event.detail.classificacao == 0)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 1)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 2)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star.png");
                }
                else if($scope.event.detail.classificacao == 3)
                {
                    angular.element(document.getElementById('star1')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star2')).attr("src","img/star_selected.png");
                    angular.element(document.getElementById('star3')).attr("src","img/star_selected.png");
                }
            };
            
            CompanyService.get().then(function(company)
            {
                if(company)
                {
                    var params = {
                            idcompany: company.data.id, 
                            idevent: $scope.event.detail.id, 
                            deviceuuid: device.uuid, 
                            classificacao: $scope.classificacao
                        };
                    CastClassification.get(params).$promise.then(successCastClassificacao, failCastClassificacao);        
                }
            });                       
        }

        $scope.sendComment = function(id)
        {
            if(angular.element(document.getElementById('detalhe-nome-val')).val()=="")
            {
                Message.alert("Preencha o nome.", "Deixe o seu comentário", "Ok");
                return;
            }
                                          
            if(angular.element(document.getElementById('detalhe-comentario-val')).val()=="")
            {
                Message.alert("Preencha o comentário.", "Deixe o seu comentário", "Ok");
                return;
            }
              
            CompanyService.get().then(function(company)
            {
                if(company)
                {
                    $.ajax(
                    {
                        type: "POST",
                        url: API_URL + '/event/castcomment/' + company.data.id + '/' + $scope.event.detail.id + '/' + device.uuid,
                        dataType: "json",
                        data: {
                            comment: angular.element(document.getElementById('detalhe-comentario-val')).val(),
                            nome: angular.element(document.getElementById('detalhe-nome-val')).val()
                        },
                        success: function(data)
                        {
                            if(data.state)
                            {
                                Message.alert("Comentário gravado com sucesso.", "Mensagem", "Ok"); 
                                angular.element(document.getElementById('detalhe-comentario-val')).val("");
                                angular.element(document.getElementById('detalhe-nome-val')).val("");
                            }
                            else
                                Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok"); 
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            Message.alert("Ups... Ocorreu um erro. Por favor, tente mais tarde.", "Mensagem", "Ok");
                        }
                    });
                }
            });   
        }

        $scope.mailto=function(email)
        {
            var title = $scope.event.detail.nome;
            var message = $scope.event.detail.resumo;
            var subject = encodeURIComponent(title);
            var body = encodeURIComponent(message);

            $window.location = "mailto:"+email+"?subject="+subject+"&body="+body;
            //$window.location.href = "mailto:info@nameit.pt?subject="+subject+"&body="+body;

        }

        $scope.AddEvent = function(e)
        {

            $elLoaderAddevent.show();
            e.stopImmediatePropagation();
            e.preventDefault();

            if(!addEvent){
                setTimeout(function(){
                    var title = $scope.event.detail.nome,
                        location = $scope.event.detail.local,
                        notes = $scope.event.detail.resumo,
                        sdArray = $filter('Date2Array')($scope.event.detail.data_inicio),
                        edArray = $filter('Date2Array')($scope.event.detail.data_fim),
                        startDate = new Date(sdArray[0],sdArray[1]-1,sdArray[2],sdArray[3],sdArray[4],sdArray[5]),
                        endDate = new Date(edArray[0],edArray[1]-1,edArray[2],edArray[3],edArray[4],edArray[5]);

                    var success = function(message) {
                        $elLoaderAddevent.hide();
                        addEvent = false;
                        Message.alert("O evento foi adicionado com sucesso ao seu calendário.", "Evento", "Ok");
                    };

                    var error = function(message) {
                        $elLoaderAddevent.hide();
                        addEvent = false;
                        Message.alert("Não foi possível adicionar o evento ao calendário, por favor, certifique as permissões de Calendário.", "Erro - Evento", "Ok");
                    };

                    if(window.plugins && window.plugins.calendar){

                        var calendar = window.plugins.calendar;

                        if(device.platform==='Android'){
                            calendar.createEvent(title,location,notes,startDate,endDate);
                            success.call();
                            $elLoaderAddevent.hide();
                            addEvent = false;
                        }
                        else{
                            calendar.createEvent(title,location,notes,startDate,endDate, success, error);
                        }
                    }

                }, 100);
            }
        }

        $scope.goMap = function(id){
            $rootScope.go('/map/'+id);
        };
    }
]);

app.controllers.controller('mapCtrl', [
    '$scope','$rootScope','$routeParams','EventsService',
    function ($scope, $rootScope, $routeParams, EventsService) {

        $scope.event = {
            map: {}
        };

        EventsService.getById($routeParams.id).then(function(event){
            $scope.$safeApply($scope, function(){
                $scope.event.map = event;
            });
        });
	}
]);

app.controllers.controller('notificationsCtrl', [
    '$scope','$rootScope','NotificationService','hexToRgb','PushNotifications','CompanyService','LastSynService','DownloadAssets','EventsService','CategorysService','PubService','RestData','$q','$timeout','RestNotifications','$route',
    function ($scope, $rootScope,NotificationService, hexToRgb, PushNotifications, CompanyService, LastSynService, DownloadAssets, EventsService, CategorysService, PubService, RestData, $q, $timeout, RestNotifications, $route) {
        var colorRgb = hexToRgb($rootScope.getColor());
        var w = Math.floor((window.innerWidth*110)/320);

        $scope.titlewidth =  {width: w + 'px'};

        $scope.data = {
            notifications: []
        };

        $scope.getStyleBg = function(read){
            return read === 0 && colorRgb!==null?{'background': 'rgba('+colorRgb.r+','+colorRgb.g+','+colorRgb.b+',.1)'}:{};
        }

        $scope.isLoading = false;

        NotificationService.getAll().then(function(records){

            $rootScope.$safeApply($scope, function(){
                $scope.data.notifications = records;
            });

            getUpdates();

        });

        var render = function () {


            NotificationService.getAll().then(function (records) {

                $rootScope.$safeApply($scope, function(){

                    $scope.data.notifications = records;

                    PushNotifications.clear();
                    NotificationService.markAllRead();


                });

            });

        }

        var getNotifications = function(idcomapny){
            var defer = $q.defer();
            var params = {id: idcomapny, token:PushNotifications.getToken()};
            RestNotifications.get(params).$promise.then(function(data){
                _loadingNotifications = false;
                if(data.state){
                    NotificationService.saveFromResource(data.data).then(function(records){

                        /*log('get NotificationService');
                        log(JSON.stringify(records));*/

                        var  c = 0;
                        if (records) {
                            records = records.data;
                            var i, n = records.length;
                            for (i = 0; i < n; i++) {
                                var record = records[i];
                                if (record.read !== 1)
                                    c++;
                            }
                        }

                        $rootScope.setBadges(c);

                        defer.resolve();
                    });
                }
            }, function(){
                _loadingNotifications = false;
                defer.resolve();
            });

            return defer.promise;
        }

        var getUpdates = function(){

            if(!_loading){
                $scope.isLoading = true;
                _loading = true;
                _lastSyn = new Date();

                var successRestData = function (resource) {
                    if(resource.state){


                        CompanyService.save(resource.data.company);
                        LastSynService.save(resource.data.lastSyn);

                        DownloadAssets(resource.data).then(function(data){

                            var deferAll = [];

                            deferAll.push(EventsService.save(data.eventos));
                            deferAll.push(CategorysService.save(data.categorias_evento));
                            deferAll.push(PubService.save(data.pub_geral));

                            $q.all(deferAll).then(function(){

                                _loading = false;
                                $scope.isLoading = false;
                                render();
                            });

                        });

                    }
                    else{

                        _loading = false;
                        $scope.isLoading = false;
                        NotificationService.markAllRead();

                    }
                };

                var failRestData = function (e) {
                    _loading = false;
                    $scope.isLoading = false;
                };


                CompanyService.get().then(function(company){

                    if(company){
                        getNotifications(company.data.id).then(function(){
                            LastSynService.get().then(function(lastSyn){
                                lastSyn = lastSyn || 0;
                                var params = {id: company.data.id, lastSyn:lastSyn, token:device.uuid};
                                RestData.get(params).$promise.then(successRestData, failRestData);
                            });
                        });
                    }
                });

            }
            else{
                //log('Already loading from another process');
            }
        };

        $scope.$on('notification', function(){
            $rootScope.setSlide('');
            $route.reload();
        });

        $scope.$on('resume', function(){
            $rootScope.setSlide('');
            $route.reload();
        });

        $scope.$on('pause', function(){
            //log('pause');
            PushNotifications.clear();
        });
	}
]);

app.controllers.controller('noconnectionCtrl', [
    '$scope','$rootScope','Message',
    function ($scope, $rootScope, Message) {
        $scope.checkConnection = function(){
            if(navigator.network.connection.type === Connection.NONE){
                Message.alert("Não é possível aceder a internet. Por favor verifique ligação.", "Mensagem", "Ok", null);
            }
            else{
                $rootScope.go('/insertcode');
            }
        }
    }
]);